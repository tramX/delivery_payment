from django.apps import AppConfig


class AppDeliveryPaymentConfig(AppConfig):
    name = 'app_delivery_payment'
