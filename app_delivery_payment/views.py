from django.shortcuts import render
from django.http import JsonResponse

from selenium import webdriver
import time
import urllib
import os


def index(request):
    return render(request, 'index.html',)


def get_price(request):
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    request_url = 'https://groozgo.ru/order/new?from={}&to={}&cargo_weight=20000'.format(urllib.request.quote(request.POST['address-from'].encode('utf-8')),
                                                                                         urllib.request.quote(request.POST['address-to'].encode('utf-8')))

    driver = webdriver.PhantomJS(executable_path=BASE_DIR + '/phantomjs/bin/phantomjs')
    driver.get(request_url)
    regbtn = driver.find_element_by_class_name('btn-pMax')
    regbtn.click()
    time.sleep(6)
    price = driver.find_element_by_name('price')

    while True:
        time_out = 6
        if price.get_attribute('value') == '':
            price = driver.find_element_by_name('price')
            time.sleep(time_out)
            time_out += 1
        else:
            break
    return JsonResponse({'price':price.get_attribute('value')})
